<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Newsy;

final class NewsyComponent extends \raciow\component\ComponentBase {

	 private $config;

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 protected function _init() {
			$this->config = $this->getComponent('raciow.Configuration')->loadConfig('component');
	 }

	 public function addNews($title, $content, $creatorID) {
			
	 }

	 public function updateNews($newsID, $newTitle, $newContent, $creatorID) {
			
	 }

	 public function getNewsPage($lastID) {
			
	 }

}

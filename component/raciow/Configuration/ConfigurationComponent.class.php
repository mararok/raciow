<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Configuration;

class ConfigurationComponent extends \raciow\component\ComponentBase {

	 const CONFIG_FILENAME = '/config.php';

	 private $configBasePath;

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function _init() {
			$this->configBasePath = $this->getAppBasePath() . '/config/';
	 }

	 public function loadConfig($configPath) {
			$configAbsolutePath = $this->configBasePath . $configPath . ConfigurationComponent::CONFIG_FILENAME;
			if (file_exists($configAbsolutePath)) {
				 $configRaw = include $configAbsolutePath;
				 if (is_array($configRaw)) {
						return new Configuration($configRaw);
				 }
			}

			return new Configuration([]);
	 }

	 public function getConfigBasePath() {
			return $this->configBasePath;
	 }

}

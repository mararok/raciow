<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Configuration;

/** Simple configuration class.
 * @package \component\raciow\Configuration
 */
class Configuration {

	 private $data;

	 public function __construct(array $data) {
			$this->data = $data;
	 }

	 public function get($key) {
			return $this->data[$key];
	 }

	 public function exists($key) {
			return isset($this->data[$key]);
	 }

}

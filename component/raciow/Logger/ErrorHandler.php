<?php
use \raciow\util\Logger as Logger;

function ErrorHandler($errno,$errstr,$errfile,$errline) {
	if (!(error_reporting()&$errno)) {
		return;
	}
	$logger = Logger::get('main');
	switch ($errno) {
		case E_USER_ERROR :
         $logger->log(Logger::FATAL_ERROR,addslashes(" FATAL ERROR: [$errno] $errstr on [$errline] in [$errfile]"));
         exit(1);
         break;

      case E_USER_WARNING :
         $logger->log(Logger::WARNING,addslashes("WARNING: [$errno] $errstr"));
         break;

      case E_USER_NOTICE :
         $logger->log(Logger::INFO,addslashes("NOTICE: [$errno] $errstr"));
         break;

      case E_PARSE :
         $logger->log(Logger::FATAL_ERROR,addslashes("PARSE: [$errno] $errstr on [$errline] in [$errfile]"));
         break;

      case E_NOTICE:
         return true;
         break;

      default :
         $logger->log(Logger::INFO,addslashes("Unknown: [$errno] $errstr on [$errline] in [$errfile]"));
         break;
   }
   
   if (!defined('DL_DEBUG_MODE'))
      echo '</br>Some fatal error !!!</br>';
   else 
      echo "</br>$errstr in $errfile,$errline</br>";
   return true;
}

set_error_handler('ErrorHandler');
?>
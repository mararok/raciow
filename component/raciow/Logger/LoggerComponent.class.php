<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Logger;

use \raciow\component;

final class LoggerComponent extends component\ComponentBase {

	 const INFO = 0;
	 const WARNING = 1;
	 const EXCEPTION = 2;
	 const FATAL = 3;
	 const SQLERROR = 4;

	 private $outputAmount = 0;
	 private $logOutputs = [];

	 function __construct(\component\ComponentEnviroment $enviroment) {
			parent::__construct($enviroment);
	 }

	 public function info($infoMessage) {
			$this->log(Logger::INFO, $infoMessage);
	 }

	 public function warning($warningMessage) {
			$this->log(Logger::WARNING, $warningMessage);
	 }

	 public function exception(\Exception $exception) {
			$this->log(Logger::EXCEPTION, (string) $exception);
	 }

	 public function sqlError($errorInfo, $sqlQueryString) {
			$message = $errorInfo . '::' . $sqlQueryString;
			$message = mysql_real_escape_string($message);

			$this->log(Logger::SQL_ERROR, $message);
	 }

	 public function fatal($fatalMessage) {
			$this->log(Logger::FATAL, $fatalMessage);
	 }

	 public function log($type, $message) {
			for ($i = 0; $i < $this->outputAmount;  ++$i) {
				 $this->logOutputs[$i]->log($type, $message);
			}
	 }

	 public function addLogOutput(LogOutput $newOutput) {
			if ($newOutput !== null) {
				 $this->logOutputs[] = $newOutput;
				 ++$this->outputAmount;
			}
	 }

	 public function _componentVersion() {
			return '0.1';
	 }

	 public function _coreVersion() {
			return '0.1';
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Logger;

interface LogOutput {

	 /**
		* @param int $type
		* @param string $message
		*/
	 public function log($type, $message);
}

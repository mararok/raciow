<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\entity;

/**
 *  @package component\raciow\ECV
 *  @subpackage entity
 */
interface EntityBuilder {

	 /** Builds Entity instance from internal data.
		* @return \raciow\entity\EntityBase
		*/
	 public function build();
}

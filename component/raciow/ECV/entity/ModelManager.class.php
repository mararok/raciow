<?php
/** @file
 * @author Andrzej Mararok Wasiak (mararok@gmail.com)
 */
namespace raciow\model;
use \raciow\application as application;
use \raciow\util as util;

/** Base class of Model Manager.
 *  @package raciow
 *  @subpackage model
 * + Has support for basic cache based on model ids.
 * + returns model instance or when it is not exists return empty model instance.
 */
abstract class ModelManager {
	private $tableName;
	private $emptyInstance;
	private $webApplication;
	
	private $cache;

	private static $createNewQuery;
	private static $getFieldsQuery;
	private static $updateFieldsQuery;
	private static $deleteQuery;
	
	/** 
	 * @param string $tableName Database store table
	 * @param \raciow\\model\Model $emptyInstance Model empty instance is returned when real doesnt exists.
	 */
	public function __construct($tableName, Model $emptyInstance, application\WebApplication $webApplication) {
		$this->tableName = $tableName;
		$this->emptyInstance = $emptyInstance;
		$this->webApplication = $webApplication;
		$this->cache = new util\Cache();
	}
	
	public static function createQueryCache(application\WebApplication $webApplication) {
		$db = $webApplication->getDB();
		$tablePrefix = $db->getTablePrefix();
		
		ModelManager::$createNewQuery = $db->prepare("INSERT $tablePrefix? (?) VALUES(?)");
		ModelManager::$getFieldsQuery = $db->prepare("SELECT ? FROM $tablePrefix? WHERE id=? LIMIT 1");
		ModelManager::$updateFieldsQuery = $db->prepare("UPDATE $tablePrefix? SET ? WHERE id=? LIMIT 1");
		ModelManager::$deleteQuery = $db->prepare("DELETE FROM $tablePrefix? WHERE id=? LIMIT 1");
	}
	
	/** Creates new model in database. 
	 * @param array $data Model data where array key is table column and array value under key is value of column.
	 */
	protected function createNewFromData($data) {
		$columns = 'name,'.implode(',',array_keys($data));
		$dataString = implode(',',$data);
		$query = ModelManager::$createNewQuery;
		if (!$query->execute(array($this->getTableName(),$columns,$dataString))) {
			// on failure
		}
	}
	
	/** 
	 * @param int $id Model id to return.
	 * @return Model instance or empty model instance.
	 */
	public function getByID($id) {
		$model  = $this->cache->get($id);
		if ($model === null) {
			$this->model = $this->loadFromID($id);
			if ($model === null) {
				$model = $this->getEmptyInstance();
			}
		} 
		
		if ($model->isValid()) {
			$this->cache->set($id,$model);
		}
		
		return $model;
	}
	
	public function getByIDs($ids) {
		$models = array();
		$len = count($ids);
		for ($i = 0; $i < $len; ++$i) {
			$models[] = getByID();
		}
		return $models;
	}
	
	/** Extending model manager class must define that method.
	 *  Loading model data from ID and create model instance.
	 *  @return Model instance or null if model with that id isnt exists.
	 */
	protected abstract function loadFromID($id);

	/** Sends query to DB for get value of column in row where id = $id.
	 * @param int $id
	 * @param string $columnName 
	 * @return string|boolean If field and model exists return column value or false.
	 */
	public function getModelField($id, $columnName) {
		$data = $this->getModelFields($id, $columnName);
		return ($data)?$data[0]:false;
	}
	
	/**
	 * @param int $id 
	 * @param string $columnName 
	 * @return string|boolean If field and model exists return column value or false.
	 */
	public function getModelFields($id, $columnsNames) {
		$query = ModelManager::$getFieldsQuery;
		if ($query->execute(array($this->getTableName(),$columnsNames,$id))) {
			return $query->fetch();
		}
		return false;
	}
	
	public function updateModelField($id, $columnName, $newValue) {
		return $this->updateModelFields($id,"$columnName=$newValue");
	}
	
	public function updateModelFields($id, $set) {
		$this->cache->remove($id);
		return ModelManager::$updateFieldsQuery->execute(array($this->getTableName(),$set,$id));
	}
	
	public function deleteModel($id) {
		$this->cache->remove($id);
		return ModelManager::$deleteQuery->execute(array($this->getTableName(),$id));
	}
	
	public function getTableName() {
		return $this->tableName;
	}
	
	public function getEmptyInstance() {
		return $this->emptyInstance;
	}
	
}

?>


<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\entity;

abstract class EntityDatabaseRepository extends \raciow\entity\EntityRepository {

	 private static $dbConnection;

	 /** @var \PDOStatement */
	 private static $insertQuery = null;

	 /** @var \PDOStatement */
	 private static $selectQuery = null;

	 /** @var \PDOStatement */
	 private static $updateQuery = null;

	 /** @var \PDOStatement */
	 private static $deleteQuery = null;

	 /** @var string */
	 private $tableName;

	 public function __construct($tableName) {
			$this->tableName = $tableName;
	 }

	 public function load($id) {
			$entity = parent::load($id);
			if ($entity === null) {
				 $this->loadFromDatabase($id);
			}
	 }

	 protected function loadFromDatabase($id) {
			
	 }

	 public function save(EntityBase $entity) {
			if (!$entity->isSaved()) {
				 if ($entity->isNew()) {
						return $this->insertNew($entity);
				 } else {
						return $this->updateInDatabase($entity);
				 }
			}
			return true;
	 }

	 private function insertNew(EntityBase $entity) {
			$columns = $this->_insertNewFieldList();
			$id = $this->dbInsert($columns, $this->extractInsertNewData($entity));
			if ($id) {
				 EntityRepository::$privateAccessor->setContextForSetter($entity);
				 EntityRepository::$privateAccessor->set('id', $id);
				 $this->addToCache($entity);
				 return true;
			}
			return false;
	 }

	 protected abstract function _insertNewFieldList();

	 public abstract function extractInsertNewData(EntityBase $entity);

	 private function updateInDatabase(EntityBase $entity) {
			$data = &$entity->_getUpdateFieldList();
			$id = $entity->getID();
			$return = $this->dbUpdate(implode(',', array_keys($data)), implode(',', $data), "id=$id LIMIT 1");
			if ($return) {
				 EntityRepository::$privateAccessor->setContextForSetterMany($entity);
				 EntityRepository::$privateAccessor->setMany($data);
				 $entity->_clearUpdateFieldList();
			}
			return $return;
	 }

	 protected function dbInsert($columns, $values) {
			if (EntityDatabaseRepository::$insertQuery->execute([$this->tableName, $columns, $values])) {
				 return EntityDatabaseRepository::$dbConnection->getLastInsertId();
			}
			return false;
	 }

	 protected function dbSelect($columns, $conditions) {
			return EntityDatabaseRepository::$insertQuery->execute([$this->tableName, $columns, $conditions]);
	 }

	 protected function dbUpdate($set, $conditions) {
			return EntityDatabaseRepository::$insertQuery->execute([$this->tableName, $set, $conditions]);
	 }

	 protected function dbDelete($conditions) {
			return EntityDatabaseRepository::$insertQuery->execute([$this->tableName, $conditions]);
	 }

	 public static function _initQueries($dbConnection, array $queries) {
			EntityDatabaseRepository::$dbConnection = $dbConnection;
			EntityDatabaseRepository::$insertQuery = $queries[0];
			EntityDatabaseRepository::$selectQuery = $queries[1];
			EntityDatabaseRepository::$updateQuery = $queries[2];
			EntityDatabaseRepository::$deleteQuery = $queries[3];
	 }

}

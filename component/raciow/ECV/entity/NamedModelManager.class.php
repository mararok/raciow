<?php
/** @file
 * @author Andrzej Mararok Wasiak (mararok@gmail.com)
 */
namespace raciow\model;

use \raciow\util as util;

/** Base class of Model Manager.
 *  @package raciow
 *  @subpackage model
 * + Has support for basic cache based on model ids.
 * + returns model instance or when it is not exists return empty model instance.
 */
abstract class NamedModelManager extends ModelManager {
	/** 
	 * @param string $tableName Database store table
	 * @param \raciow\\model\NamedModel $emptyInstance Model empty instance is returned when real doesnt exists.
	 */
	public function __construct($tableName, NamedModel $emptyInstance) {
		parent::__construct($tableName,$emptyInstance);
	}
	
	/** Creates new model in database when name doesnt exists. 
	 * @param string $name Model uniq. name
	 * @param array $data Model data where array key is table column and array value under key is value of column.
	 */
	protected function createNewIfNotExists($name, $data) {
		if (!$this->getByName($name)->isValid()) {
			$data['name'] = $name;
			$this->createNewFromData($data);
		} 
	}
		
	public function getByName($name) {
		$model = $this->loadFromName($name);
		if ($model === null) {
			$model = $this->getEmptyInstance();
		}
		
		if ($model->isValid()) {
			$this->cache->set($model->getID(),$model);
		}
		
		return $model;
	}

	protected abstract function loadFromName($name);
	
}

?>


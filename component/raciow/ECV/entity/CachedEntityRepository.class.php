<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\entity;

class CachedEntityRepository implements EntityRepository {

	 private $cache = [];

	 public function load($id) {
			if ($this->isCached($id)) {
				 return $this->cache[$id];
			}

			return null;
	 }

	 public function save(EntityBase $entity) {
			if (!$this->isCached($entity->getID())) {
				 $this->cache[$entity->getID()] = $entity;
			}
	 }

	 public function isCached($id) {
			return isset($id);
	 }

	 public function delete(EntityBase $entity) {
			if ($this->isCached($entity->getID())) {
				 unset($this->cache[$entity->getID()]);
			}
	 }

}

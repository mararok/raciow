<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\entity;

/**
 *  @package component\raciow\ECV
 *  @subpackage entity
 */
interface EntityRepository {

	 /** Load entity from internal repository storage.
		* @param int $id
		* @return \raciow\entity\EntityBase|null 
		*/
	 public function findById($id);
}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\entity;

/** Base class for entities
 *  @package component\raciow\ECV
 *  @subpackage entity
 */
abstract class EntityBase {

	 private $id;

	 public function __construct($id = 0) {
			$this->id = (int) $id;
	 }

	 public function getId() {
			return $this->id;
	 }

	 private function __clone() {
			trigger_error("Entity object can't be clone ", E_USER_ERROR);
	 }

}

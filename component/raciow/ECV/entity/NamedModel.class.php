<?php
/** @file
 * @author Andrzej Mararok Wasiak (mararok@gmail.com)
 */

namespace raciow\model;

/** 
 *  @package raciow
 *  @subpackage model
 */
class NamedModel extends Model {
	private $name;
	
	public function __construct($id, $name, NamedModelManager $modelManager) {
		parent::__construct($id, $modelManager);
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
}

?>
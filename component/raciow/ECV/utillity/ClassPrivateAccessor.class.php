<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\utillity;

class ClassPrivateAccessor {

	 /** @var \Closure */
	 private $contexGetter;

	 /** @var \Closure */
	 private $contextGetterMany;

	 /** @var \Closure */
	 private $contexSetter;

	 /** @var \Closure */
	 private $contextSetterMany;

	 public function __construct() {
			$this->contextGetter = function($fieldName) {
				 return $this->$fieldName;
			};

			$this->contextGetterMany = function(array $fieldsList) {
				 $values = [];
				 for ($i = 0, $len = count($fieldsList); $i < $len; ++$i) {
						$values[] = $fieldsList[$i];
				 }
				 return $values;
			};

			$this->contextSetter = function($fieldName, $newValue) {
				 $this->$fieldName = $newValue;
			};

			$this->contextSetterMany = function(array $values) {
				 foreach ($values as $field => $value) {
						$this->$field = $value;
				 }
			};
	 }

	 public function getGetterFor($context) {
			return $this->contextGetter->bindTo($context, $context);
	 }

	 public function getGetterManyFor($context) {
			return $this->contextSetterMany->bindTo($context, $context);
	 }

	 public function getSetterFor($context) {
			return $this->contextSetter->bindTo($context, $context);
	 }

	 public function getSetterManyFor($context) {
			return $this->contextSetterMany->bindTo($context, $context);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\view;

/**
 *  @package component\raciow\ECV
 *  @subpackage view
 */
abstract class ViewBase {

	 public abstract function render();
}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\controller;

use \raciow\app;
use \component\raciow\ECV\action;

/** Controllers main driver
 *  Processing outside and inside controller action requests
 *  @package raciow
 *  @subpackage controller
 */
class ControllerRouter {

	 const CONTROLLER_NAME_INDEX = 0;
	 const ACTION_NAME_INDEX = 1;

	 private $defaultControllerName;
	 private $controllersNamespace;
	 private $app;

	 public function __construct($defaultControllerName, $controllersNamespace, app\AppBase $app) {
			$this->defaultControllerName = $defaultControllerName;
			$this->controllersNamespace = $controllersNamespace;
			$this->app = $app;
	 }

	 /** Proccesing outside controller action requests.
		* When no request parameteres exec default action from default app controller.
		* uri format: /[controllerName]/[[actionName][/actionParameters]]
		*/
	 public function processOutsideRequest() {
			$uri = ltrim($_SERVER['REQUEST_URI'], '/');
			$uriParameters = explode('/', urldecode($uri));
			$uriParametersAmount = count($uriParameters);
			$controllerName = '';
			$actionContextBuilder = null;
			if ($uriParametersAmount > 1) {
				 $actionContextBuilder = $this->extractRequestParameters($uriParameters, $uriParametersAmount, $controllerName);
			} else {
				 $controllerName = $this->defaultControllerName;
				 $actionContextBuilder = (new action\ActionContextBuilder())
								 ->name(ControllerBase::DEFAULT_ACTION_NAME)
								 ->parameters(new action\ActionParameters(action\ActionParameters::TYPE_GET, []));
			}
			$this->processActionRequest($controllerName, $actionContextBuilder);
	 }

	 private function extractRequestParameters(array &$uriParameters, $uriParametersAmount, &$controllerName) {
			$sanitizer = $this->app->getComponent('raciow.Sanitizer');
			$controllerName = $sanitizer->string($uriParameters[ControllerRouter::CONTROLLER_NAME_INDEX]);
			return (new action\ActionContextBuilder())
											->name(($uriParametersAmount > 2) ?
																			$sanitizer->string($uriParameters[ControllerRouter::ACTION_NAME_INDEX]) :
																			ControllerBase::DEFAULT_ACTION_NAME)
											->parameters($this->extractActionParameters($uriParameters, $uriParametersAmount));
	 }

	 private function extractActionParameters(array &$uriParameters, $uriParametersAmount) {
			$actionType = ($_SERVER['REQUEST_METHOD'] == 'POST') ? action\ActionParameters::TYPE_POST : action\ActionParameters::TYPE_GET;
			$actionParameters = null;
			if ($actionType == action\ActionParameters::TYPE_POST) {
				 $actionParameters = $_POST;
				 unset($_POST);
			} else {
				 $actionParameters = [];
				 for ($i = 3; $i < $uriParametersAmount; ++$i) {
						$actionParameters[] = $uriParameters[$i];
				 }
			}

			return new action\ActionParameters($actionType, $actionParameters);
	 }

	 public function processActionRequest($controllerName, action\ActionContextBuilder $actionContextBuilder) {
			$controllerName = $this->controllersNamespace . $controllerName . 'Controller';
			$controller = new $controllerName();
			$controller->setApp($this->app);
			$actionContext = $actionContextBuilder->object($controller)->build();
			$action = new action\Action($actionContext);
			echo $action->execute();
	 }

}

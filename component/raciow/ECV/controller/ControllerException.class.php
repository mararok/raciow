<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\controller;

/**
 *  @package component\raciow\ECV
 *  @subpackage controller
 */
class ControllerException {

	 private $controllerName;
	 private $action;
	 private $message;

	 public function __construct($controllerName, Action $action, $message) {
			$this->controllerName = $controllerName;
			$this->action = $action;
			$this->message = $message;
	 }

	 public function getControllerName() {
			return $this->controllerName;
	 }

	 public function getAction() {
			return $action;
	 }

	 public function getMessage() {
			return $this->message;
	 }

	 public function __toString() {
			return sprinf('CN:%s A:%d  M:%s', $this->controllerName, $this->action, $this->message);
	 }

}

?>
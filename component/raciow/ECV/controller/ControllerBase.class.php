<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\controller;

use \raciow\app;
use \component\raciow\ECV\action;

/** Base for all app controllers.
 *  @package component\raciow\ECV
 *  @subpackage controller
 */
abstract class ControllerBase implements action\Actionable {

	 const DEFAULT_ACTION_NAME = 'default';

	 private $app;

	 public function defaultAction(action\ActionParameters $parameters) {
			
	 }

	 public function getComponent($name) {
			return $this->app->getComponent($name);
	 }

	 public function getApp() {
			return $this->app;
	 }

	 public function setApp(app\AppBase $newApp) {
			$this->app = $newApp;
	 }

}

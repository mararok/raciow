<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV;

/**
 *  @package component\raciow\ECV
 */
class ECVComponent extends \raciow\component\ComponentBase {

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

/** Action for any class implements Actionable interface.
 *  @package \component\raciow\ECV
 *  @subpackage action
 *  It allows you to perform the associated action in a given context.
 */
class Action {

	 /** @var \component\raciow\ECV\action\ActionContext */
	 private $context;

	 public function __construct(ActionContext $context) {
			$this->context = $context;
	 }

	 public function getContext() {
			return $this->context;
	 }

	 /** Execute action with current context.
		* @return \component\raciow\ECV\action\ActionResult
		*/
	 public function execute() {
			$context = $this->context;
			if ($context !== null && $context->isValid()) {
				 $result = $context->getActionObject()->{$context->getActionName() . 'Action'}($context->getActionParameters());
				 return ($result instanceof ActionResult) ? $result : new EmptyActionResult();
			}
			return new EmptyActionResult();
	 }

}

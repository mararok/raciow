<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

use ReflectionClass;

/** Action enviroment context.
 *  @package component\raciow\ECV
 *  @subpackage action
 *  
 */
class ActionContext {

	 private $actionName;
	 private $actionParameters;
	 private $actionObject;
	 private $valid = false;

	 public function __construct($actionName, ActionParameters $parameters, Actionable $actionObject) {
			$this->actionName = $actionName;
			$this->actionParameters = $parameters;
			$this->actionObject = $actionObject;
	 }

	 /**
		* @return string
		*/
	 public function getActionName() {
			return $this->actionName;
	 }

	 /**
		* @return \component\raciow\ECV\action\ActionParameters
		*/
	 public function getActionParameters() {
			return $this->actionParameters;
	 }

	 /**
		* @return \component\raciow\ECV\action\Actionable
		*/
	 public function getActionObject() {
			return $this->actionObject;
	 }

	 /** Returns true if object can execute action.
		*  @return bool 
		*/
	 public function isValid() {
			if (!$this->valid) {
				 $this->checkValid();
			}
			return $this->valid;
	 }

	 private function checkValid() {
			$actionMethodName = $this->actionName . 'Action';
			$actionMethodFormat = $actionMethodName . 'Format';
			$class = new ReflectionClass(get_class($this->actionObject));
			if ($class->hasMethod($actionMethodName) || $class->hasMethod($actionMethodFormat)) {
				 $this->valid = true;
			} else {
				 $this->valid = false;
			}

			unset($class);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

/** Array result of action execution.
 *  @package raciow
 *  @subpackage action
 */
class ArrayActionResult extends ActionResult {

	 private $result = [];

	 /** Sets part result to value.
		* @param string $key Name of result part.
		* @param mixed $value
		*/
	 function set($key, $value) {
			$this->result[$key] = $value;
	 }

	 /** Returns part result value.
		* @param string Name of result part.
		* @return mixed
		*/
	 function get($key) {
			return $this->result[$key];
	 }

	 /** Returns raw result array.
		* @return array
		*/
	 function getAll() {
			return $this->result;
	 }

	 /**
		* @return string
		*/
	 function __toString() {
			return var_export($this->result, true);
	 }

}

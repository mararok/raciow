<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

/** Action for any class implements Actionable interface.
 *  @package component\raciow\ECV
 *  @subpackage action
 */
interface Actionable {

	 public function defaultAction(ActionParameters $parameters);
}

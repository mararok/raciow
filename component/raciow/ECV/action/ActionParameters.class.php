<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

class ActionParameterException extends \raciow\ExceptionBase {

	 private $parameterName;

	 function __construct($message, $parameterName) {
			parent::__construct($message);
			$this->parameterName = $parameterName;
	 }

	 public function getParameterName() {
			return $this->parameterName;
	 }

}

class ActionParameterInvalidTypeException extends ActionParameterException {

	 private $paramterValue;
	 private $expectedTypeName;

	 public function __construct($parameterName, $parameterValue, $expectedTypeName) {
			parent::__construct("Parameter '$parameterName' has invalid type, expectd '$expectedTypeName'", $parameterName);
			$this->paramterValue = $parameterValue;
			$this->expectedTypeName = $expectedTypeName;
	 }

	 public function getParameterValue() {
			return $this->parameterValue;
	 }

	 public function getExpectedTypeName() {
			return $this->expectedTypeName;
	 }

}

class ActionParameterNotExistsException extends ActionParameterException {

	 public function __construct($parameterName) {
			parent::__construct("Parameter '$parameterName' isn't exists ", $parameterName);
	 }

}

/**
 *  @package raciow
 *  @subpackage action
 */
class ActionParameters {

	 const TYPE_GET = 0;
	 const TYPE_POST = 1;
	 const TYPE_INNER = 2;

	 private $requestType;
	 private $parameters;

	 /**
		* @param array $parameters Array with parameters for action.
		*/
	 function __construct($requestType, $parameters = []) {
			$this->requestType = $requestType;
			$this->parameters = $parameters;
	 }

	 public function isGET() {
			return $this->getType() === Action::TYPE_GET;
	 }

	 public function isPOST() {
			return $this->getType() === Action::TYPE_POST;
	 }

	 public function isINNER() {
			return $this->getType() === Action::TYPE_INNER;
	 }

	 /** Returns parameter as integer value.
		* @return int
		*/
	 function asInt($parameterName) {
			$parameter = $this->get($parameterName);
			if (is_numeric($parameter)) {
				 return (int) $parameter;
			}
			throw new ActionParameterInvalidTypeException($parameterName);
	 }

	 /** Returns parameter as float value;
		* @return float
		*/
	 function asFloat($parameterName) {
			$parameter = $this->get($parameterName);
			if (is_numeric($parameter)) {
				 return (float) $parameter;
			}
			throw new ActionParameterInvalidTypeException($parameterName);
	 }

	 /** Returns parameter as boolean value;
		* @return bool
		*/
	 function asBool($parameterName) {
			$parameter = $this->get($parameterName);
			if (is_bool($parameter)) {
				 return (bool) $parameter;
			}
			throw new ActionParameterInvalidTypeException($parameterName);
	 }

	 /** Returns parameter as raw string - not safe 
		* @return string
		*/
	 function asString($parameterName) {
			return (string) $this->get($parameterName);
	 }

	 private function get($parameterName) {
			if (array_key_exists($parameterName, $this->parameters)) {
				 return $this->parameters[$parameterName];
			}
			throw new ActionParameterNotExistsException($parameterName);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\ECV\action;

class ActionContextBuilder {

	 private $actionName;
	 private $actionParameters;
	 private $actionObject;

	 /**
		* @param string $name
		*/
	 public function name($name) {
			$this->actionName = $name;
			return $this;
	 }

	 /**
		* @param \component\raciow\ECV\action\ActionParameters $parameters
		*/
	 public function parameters(ActionParameters $parameters) {
			$this->actionParameters = $parameters;
			return $this;
	 }

	 /**
		* @param \component\raciow\ECV\action\Actionable $object
		*/
	 public function object($object) {
			$this->actionObject = $object;
			return $this;
	 }

	 public function build() {
			return new ActionContext($this->actionName, $this->actionParameters, $this->actionObject);
	 }

}

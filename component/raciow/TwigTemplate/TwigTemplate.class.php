<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\TwigTemplate;

use component\raciow\Template\Template;
use component\raciow\Template\TemplateEngine;

class TwigTemplate implements Template {

	 private $name;
	 private $internalTemplate;
	 private $engine;

	 public function __construct($name, $internalTemplate, TemplateEngine $engine) {
			$this->name = $name;
			$this->internalTemplate = $internalTemplate;
			$this->engine = $engine;
	 }

	 public function render(array $data) {
			return $this->internalTemplate->render($data);
	 }

	 public function getName() {
			return $this->name;
	 }

	 public function getEngine() {
			return $this->engine;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\TwigTemplate;

use component\raciow\Template\TemplateEngine;
use raciow\component\ComponentBase;
use raciow\component\ComponentManager;

final class TwigTemplateComponent extends ComponentBase implements TemplateEngine {

	 /** @var array */
	 private $cache;

	 /** @var Twig_Environment */
	 private $twig;

	 public function __construct(ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function _init() {
			$config = $this->getComponent('raciow.Configuration')->loadConfig('component/raciow/Template');
			$appBasePath = $this->getAppBasePath();
			require_once $appBasePath . '/lib/Twig/Autoloader.php';
			\Twig_Autoloader::register();
			$loader = new \Twig_Loader_Filesystem($appBasePath . '/templates');

			$this->twig = new \Twig_Environment($loader, array(
					'debug' => true,
					'cache' => ( ($config->exists('cache') && $config->get('cache')) ? $appBasePath . '/templates_c' : false ),
					'autoescape' => ($config->exists('autoescape')) ? $config->get('autoescape') : false
			));

			$this->cache = [];
	 }

	 public function clearCache() {
			$this->cache = [];
			$this->twig->clearCacheFiles();
	 }

	 public function load($path) {
			$template = $this->cache[$path];
			if (!isset($template)) {
				 $template = new TwigTemplate($path, $this->twig->loadTemplate($path), $this);
				 $this->cache[$path] = $template;
			}
			return $template;
	 }

	 public function render($path, array $data) {
			return $this->load($path)->render($data);
	 }

}

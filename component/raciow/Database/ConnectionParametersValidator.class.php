<?php
/** @file
 * @author Andrzej Mararok Wasiak (mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow
 * @subpackage Database
 */
 
class ConnectionParametersValidator {
	private $parameters;
	
	private $validLog = [];
	
	public function __construct(array $parameters) {
		$this->parameters = $parameters;
		$this->validParameters();
	}
	
	private function validParameters() {
		$this->parameters['engine'] = (isset($this->parameters['engine']))? $this->parameters['engine'] : MYSQL_ENGINE_TYPE;
		if ($this->parameters['engine'] == DB::MYSQL_ENGINE_TYPE || $this->parameters['engine'] == DB::POSTGRESQL_ENGINE_TYPE) {
			$this->validLog[] = "Unsuported database engine: $engine";
		} 
		
		$this->parameters['tablePrefix'] = (isset($this->parameters['tablePrefix']))?$this->parameters['tablePrefix']:'';
		$this->parameters['host'] = (isset($this->parameters['host']))? $this->parameters['host'] : 'localhost';
		
		if (isset($this->parameters['dbname'])) {
			$this->validLog[] = 'No database name sets.';
		}
		
		if (isset($this->parameters['username'])) {
			$this->validLog[] = 'No database username sets.';
		}
		
		if (isset($this->parameters['password'])) {
			$this->validLog[] = 'No database user password sets.';
		}
	}
	
	public function getCheckedParamters() {
		return $this->parameters;
	}	
	public function getValidLog() {
		return $this->validLog;
	}
	
	public function isValid() {
		return count($this->validLog) === 0;
	}
}

?>
<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
class UpdateQueryObject extends ConditionalQueryObject {

	 private $set;

	 public function __construct($tableName, DbConnection $dbConnection) {
			parent::__construct($dbConnection);
			$this->tableName = $tableName;
	 }

	 public function set($set) {
			$this->set = $set;
			return $this;
	 }

	 public function toSql() {
			return 'UPDATE ' . $this->getPrefixedTableName() . ' SET ' . $this->set . parent::toSql();
	 }

	 public function exec() {
			return $this->dbConnection->exec($this->toSql());
	 }

}

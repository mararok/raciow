<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database
 */
class DbConnectionFactory {

	 /** Creates database connection.
		* @param array $cp Connection parameters(engine,host,username,password,tablePrefix).
		* @throws \PDOException When some parameters isn't correct or can't initialize conection.
		*/
	 public static function newConnection(array $cp) {
			if (!$cp['checked']) {
				 $parameterValidator = new ConnectionParametersValidator($cp);
				 if (!$parameterValidator->isValid()) {
						throw new \PDOException(
						'Connection parameters invalid: ' . var_export($parameterValidator->getValidLog(), true)
						);
				 }

				 $cp = $parameterValidator->getCheckedParameters();
			}

			$connectionString = sprintf('%s:host=%s;dbname=%s', $cp['engine'], $cp['host'], $cp['dbname']);

			$connection = new \PDO($connectionString, $cp['username'], $cp['password']);
			$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_NUM);
			return new DbConnection($connection, $cp['tablePrefix']);
	 }

}

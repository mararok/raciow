<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
abstract class ConditionalQueryObject extends QueryObject {

	 protected $conditions;
	 protected $limit;

	 public function __construct(DbConnection $dbConnection) {
			parent::__construct($dbConnection);
	 }

	 public function where($conditions) {
			$this->conditions = $conditions;
			return $this;
	 }

	 public function limit($limit) {
			$this->limit = $limit;
			return $this;
	 }

	 public function toSql() {
			return ((isset($this->conditions)) ? ' WHERE ' . $this->conditions : ' ') .
							((isset($this->limit)) ? ' LIMIT ' . $this->limit : ' ');
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
class InsertQueryObject extends QueryObject {

	 private $columns;
	 private $values;

	 public function __construct(DbConnection $dbConnection) {
			parent::__construct($dbConnection);
	 }

	 /**
		* @param string $tableName 
		* @param string $columns List format: columnName1,columnName2,columnNameN
		* @return \component\raciow\Database\InsertQueryObject
		*/
	 public function into($tableName, $columns) {
			$this->tableName = $tableName;
			$this->columns = $columns;
			return $this;
	 }

	 /**
		* @param string values List format: (value1,value2,valueN)[,(next row values)]
		* @return \component\raciow\Database\InsertQueryObject
		*/
	 public function values($values) {
			$this->values = $values;
			return $this;
	 }

	 public function toSql() {
			return 'INSERT INTO ' . $this->getPrefixedTableName() .
							((isset($this->columns)) ? ' (' . $this->columns : ') ') .
							((isset($this->values)) ? ' ' . $this->values : ' ');
	 }

	 public function exec() {
			if ($this->dbConnection->exec($this->toSql())) {
				 return $this->dbConnection->getLastInsertId();
			}
	 }

}

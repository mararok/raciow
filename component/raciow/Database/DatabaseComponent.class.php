<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package \component\raciow\Database
 */
final class DatabaseComponent extends \raciow\component\ComponentBase {

	 private $dbConnection;

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function _init() {
			$config = $this->getComponent('raciow.Configuration')->loadConfig('component/raciow/Database');
			$this->dbConnection = DbConnectionFactory::newConnection($config);
	 }

	 public function getConnection() {
			return $this->dbConnection;
	 }

}

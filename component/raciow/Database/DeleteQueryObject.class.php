<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
class UpdateQueryObject extends ConditionalQueryObject {

	 public function __construct(DbConnection $dbConnection) {
			parent::__construct($dbConnection);
	 }

	 public function from($tableName) {
			$this->tableName = $tableName;
			return $this;
	 }

	 public function toSql() {
			return 'DELETE FROM ' . $this->getPrefixedTableName() . parent::toSql();
	 }

	 public function exec() {
			return $this->dbConnection->exec($this->toSql());
	 }

}

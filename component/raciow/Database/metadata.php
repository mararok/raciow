<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

return raciow\component\ComponentMetadataBuilder::component('raciow', 'Database', '0.1');

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
class SelectQueryObject extends ConditionalQueryObject {

	 private $columns;
	 private $orderBy;

	 public function __construct($columns, DbConnection $dbConnection) {
			parent::__construct($dbConnection);
			$this->columns = $columns;
	 }

	 public function from($tableName) {
			$this->tableName = $tableName;
			return $this;
	 }

	 public function orderBy($orderBy) {
			$this->orderBy = $orderBy;
			return $this;
	 }

	 public function toSql() {
			return 'SELECT ' . $this->columns . ' FROM ' . $this->getPrefixedTableName() .
							((isset($this->conditions)) ? ' WHERE ' . $this->conditions . ' ' : ' ') .
							((isset($this->orderBy)) ? ' ORDER BY ' . $this->orderBy . ' ' : ' ') .
							((isset($this->limit)) ? ' LIMIT ' . $this->limit : '');
	 }

	 public function exec() {
			$query = $this->dbConnection->query($this->toSql());
			$rows = $query->fetchAll();
			$query->closeCursor();
			return $rows;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database\schema;

class TableSchema {

	 private $schema;

	 public function __construct($schema) {
			$this->schema = $schema;
	 }

	 public function getForPrefix($tablePrefix) {
			return str_replace($this->schema, $tablePrefix, '_PREFIX_');
	 }

}

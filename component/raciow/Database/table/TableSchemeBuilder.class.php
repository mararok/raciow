<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

class TableSchemeBuilder {

	 private $tableName;
	 private $fields = [];
	 private $primaryKeyName;
	 private $indexes = [];

	 public function name($tableName) {
			$this->tableName = $tableName;
	 }

	 public function field(TableField $field) {
			$this->fields[$field->getName()] = $field;
	 }

	 public function primaryKey($name) {
			$this->primaryKeyName = $name;
	 }

}

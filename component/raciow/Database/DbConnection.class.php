<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
class DbConnection {

	 const MYSQL_ENGINE_TYPE = 'mysql';
	 const POSTGRESQL_ENGINE_TYPE = 'pgsql';

	 private $connection;
	 private $tablePrefix;

	 public function __construct(\PDO $pdoConnection, $tablePrefix) {
			$this->connection = $pdoConnection;
			$this->tablePrefix = $tablePrefix;
	 }

	 /**
		* @return \component\Database\InsertQueryObejct New Query Object. 
		*/
	 public function insert() {
			return new InsertQueryObject($this);
	 }

	 /**
		* @param string $columns Format: <columnName>[,<columnName>]
		* @return \component\Database\SelectQueryObejct New Query Object. 
		*/
	 public function select($columns) {
			return new SelectQueryObject($columns, $this);
	 }

	 /**
		* @param string $tableName Name without prefix
		* @return \component\Database\UpdateQueryObejct New Query Object. 
		*/
	 public function update($tableName) {
			return new UpdateQueryObject($tableName, $this);
	 }

	 /**
		* @return \component\Database\DeleteQueryObejct New Query Object. 
		*/
	 public function delete() {
			return new DeleteQueryObject($this);
	 }

	 /** Removes all sql commnds, protect for SQL injection
		* @param string $string
		* @return string
		*/
	 public function quote($string) {
			return $this->connection->quote($string);
	 }

	 public function executeQuery($sql) {
			return $this->connection->exec($sql);
	 }

	 /**
		* @param string $sql SQL query string
		* @return \PDO\PDOStatement query statement.
		*/
	 public function query($sql) {
			return $this->connection->query($sql);
	 }

	 /**
		* @param string $sql SQL query string
		* @return \PDO\PDOStatement prepared query statement.
		*/
	 public function prepare($sql) {
			return $this->connection->prepare($sql);
	 }

	 /**
		* @return int 
		*/
	 public function getLastInsertId() {
			return $this->connection->lastInsertId();
	 }

	 /**
		* @param string $tableName
		* @return string
		*/
	 public function getPrefixedTableName($tableName) {
			return $this->getTablePrefix() . $tableName;
	 }

	 /**
		* @return string
		*/
	 public function getTablePrefix() {
			return $this->tablePrefix;
	 }

}

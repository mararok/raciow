<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Database;

/**
 * @package component\raciow\Database;
 */
abstract class QueryObject {

	 /** @var string */
	 protected $tableName;

	 /** @var \component\raciow\Database\DbConnection */
	 protected $dbConnection;

	 public function __construct(DbConnection $dbConnection) {
			$this->dbConnection = $dbConnection;
	 }

	 protected function getPrefixedTableName() {
			return $this->dbConnection->getPrefixedTableName($this->tableName);
	 }

	 public abstract function toSql();

	 public abstract function exec();
}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class PasswordHash implements component\raciow\ECV\entity\ValueObject {

	 const HASH_ALGO = 'sha512';

	 private $phash;
	 private $psalt;

	 public function __construct($hash, $salt) {
			$this->phash = $hash;
			$this->psalt = $salt;
	 }

	 /**
		* @param string $password Password to hash.
		* @return PasswordHash
		*/
	 public static function generateFromPassword($password) {
			$salt = PasswordHash::generateSalt();
			return new PasswordHash(PasswordHash::generateHash($password, $salt), $salt);
	 }

	 public static function generateSalt() {
			return hash(PasswordHash::HASH_ALGO, uniqid(mt_rand(1, mt_getrandmax()), true));
	 }

	 public static function generateHash($password, $salt) {
			return hash(PasswordHash::HASH_ALGO, $password . $salt);
	 }

	 /**
		* @param string $password Password to chech
		* @return bool true if password after transform is same as that password hash
		*/
	 public function checkPassword($password) {
			return $this->phash == PasswordHash::generateHash($password, $this->psalt);
	 }

	 public function __toString() {
			return $this->phash . ',' . $this->psalt;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

use \raciow\component;
use \component\raciow\ECV;

class UserComponent extends component\ComponentBase implements ECV\EntityService {

	 private $userRepository;

	 public function __construct(component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function setUserRepository(UserRepository $newUserRepository) {
			$this->userRepository = $newUserRepository;
	 }

}

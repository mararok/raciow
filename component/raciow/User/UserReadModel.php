<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class UserReadModel {

	 public $name;
	 public $email;
	 public $activated;
	 public $banned;
	 public $registered;

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class User extends \component\raciow\ECV\entity\EntityBase {

	 private $name;
	 private $email;
	 private $activated;
	 private $banned;
	 private $registered;

	 public function __construct($id = 0) {
			parent::__construct($id);
	 }

	 public function getName() {
			return $this->name;
	 }

	 public function getEmail() {
			return $this->email;
	 }

	 public function isActivated() {
			return $this->activated;
	 }

	 public function active() {
			$this->activated = true;
	 }

	 public function isBanned() {
			return $this->banned;
	 }

	 public function ban() {
			$this->banned = true;
	 }

	 public function uban() {
			$this->banned = false;
	 }

	 public function getRegistered() {
			return $this->registered;
	 }

}

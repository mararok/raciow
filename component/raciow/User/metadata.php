<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

return raciow\COMPONENT_META('raciow', 'User', '0.1')->dependencyList(['raciow.Session|0.1', 'raciow.EventBus|0.1']);

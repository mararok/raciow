<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class UserFactory {

	 // $id,name,email,activated,banned,registered.
	 public static function newFromData(array $data) {
			return new User($data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

interface UserDAO {

	 public function create($name, $email, PasswordHash $password);

	 public function update($id, $activated, $banned);

	 public function updatePassword($id, PasswordHash $newPassword);

	 public function getPassword($id);

	 function delete($id);
}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class AuthUser extends \component\raciow\ECV\entity\EntityBase {

	 private $passwordHash;

	 public function __construct($id, PasswordHash $password) {
			parent::__construct($id);
			$this->passwordHash = $password;
	 }

	 public function setPassword($password) {
			$this->passwordHash = PasswordHash::generateFromPassword($password);
	 }

	 public function checkPassword($password) {
			return $this->passwordHash->checkPassword($password);
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class UserRepository implements \component\raciow\ECV\entity\EntityRepository {

	 private $userDAO;

	 public function __construct(UserDAO $userDAO) {
			$this->userDAO = $userDAO;
	 }

	 public function create($name, $email, $password) {
			return $this->userDAO->create($name, $email, PasswordHash::generateFromPassword($password));
	 }

	 public function findByID($id) {
			$data = $this->userDAO->findByID($id);
			return UserFactory::newFromData($data);
	 }

	 public function findByNameOrEmail($name, $email) {
			$data = $this->userDAO->findByNameOrEmail($name, $email);
			return UserFactory::newFromData($data);
	 }

	 public function update(User $entity) {
			$this->userDAO->update($entity->getID(), $entity->isActivated(), $entity->isBanned());
	 }

	 public function delete(User $entity) {
			$this->userDAO->delete($entity->getID());
	 }

	 public function updatePassword($id, $newPassword) {
			$this->userDAO->updatePassword($id, $newPassword);
	 }

	 public function getPassword($id) {
			$data = $this->userDAO->getPassword($id);
			return new PasswordHash($data[0], $data[1]);
	 }

}

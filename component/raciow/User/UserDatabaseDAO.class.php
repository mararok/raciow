<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\User;

class UserDatabaseDAO implements UserDAO {

	 /** @var \component\raciow\Database\DbConnection */
	 private $db;
	 private $tableName = "Users";

	 public function __construct(\component\raciow\Database\DbConnection $db) {
			$this->db = $db;
	 }

	 public function create($name, $email, PasswordHash $password) {
			$this->db->insert()->into($this->tableName, 'name,email,phash,psalt')
							->values("$name,$email,$password");
	 }

	 public function update($id, $activated, $banned) {
			
	 }

	 public function updatePassword($id, $newPassword) {
			
	 }

	 public function getPassword($id) {
			
	 }

	 public function delete($id) {
			
	 }

}

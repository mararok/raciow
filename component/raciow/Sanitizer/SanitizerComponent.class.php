<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Sanitizer;

class SanitizerComponent extends \raciow\component\ComponentBase {

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function email($email) {
			return \filter_var($email, \FILTER_SANITIZE_EMAIL);
	 }

	 public function string($string) {
			return \filter_var($string, \FILTER_SANITIZE_STRING);
	 }

	 public function url($url) {
			return \filter_var($url, \FILTER_SANITIZE_URL);
	 }

}

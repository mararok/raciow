<?php

/** @file
 * @author Andrzej Mararok Wasiak (mararok@gmail.com)
 */
 
namespace raciow\language;

/** Language data class
 *  @package raciow
 *  @subpackage view
 */
abstract class Language {
	const FILE_EXT = '.lang';
	const EMPTY_STRING = '^!EMPTY STRING!^';
	private $data = array();
	private $languagesFolder;
	private $langPrefix;
	private $filename;
	private $defaultLanguage = null;
	
	public function __construct() {
	}
	
	public function loadLanguage($languagesFolder, $langPrefix, $filename) {
		$this->languagesFolder = $languagesFolder;
		$this->langPrefix = $langPrefix;
		$this->filename = $filename;
		$langFilename = $languagesFolder.$langPrefix.$filename.Language::FILE_EXT;
		if (file_exists($langFilename)) {
			$lang = include $langFilename;
			return $this->setData($lang);
		}
		
		return false;
	}
	
	public function setData($langData) {
		if (is_array($langData)) {
			$this->data = $langData;
			return true;
		} 
		
		return false;
	}
	
	public function getString($key) {
		return (isset($data[$key]))?$data[$key]:$this->getDefaultString($key);
	}
	
	public function getDefaultString($key) {
		$string = ($this->hasDefaultLanguage())?$this->defaultLanguage->getString($key):Language::EMPTY_STRING;
		return ($string);
	}
	
	public function setDefaultLanguage(Language $newDefaultLanguage) {
		$this->defaultLanguage = $newDefaultLanguage;
	}
	
	public function hasDefaultLanguage() {
		return ($this->defaultLanguage !== null);
	}

}
?>
<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Template;

/** Generic template file interface
 *  @package component\raciow\Template
 */
class TemplateComponent extends \raciow\component\ComponentBase implements TemplateEngine {

	 private $currentEngine;

	 public function __construct(\raciow\component\ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

	 public function _init() {
			$config = $this->getComponent('raciow.Configuration')->loadConfig('component/raciow/Template');
			$engineName = ($config->exists('engine')) ? $config->get('engine') : 'Null';
			if ($engineName === 'Null') {
				 $this->setEngine(new NullTemplateEngine());
			} else {
				 try {
						$engineComponent = $this->getComponent($engineName);
						if ($engineComponent instanceof TemplateEngine) {
							 $this->setEngine($engineComponent);
						} else {
							 $this->setEngine(new NullTemplateEngine());
						}
				 } catch (MissingComponentException $e) {
						$this->setEngine(new NullTemplateEngine());
				 }
			}
	 }

	 private function setEngine(TemplateEngine $newEngine) {
			$this->currentEngine = $newEngine;
	 }

	 public function load($path) {
			return $this->currentEngine->loadTemplate($path);
	 }

	 public function render($path, array $data) {
			return $this->currentEngine->render($path, $data);
	 }

	 public function clearCache() {
			$this->currentEngine->clearCache();
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Template;

/**
 *  @package component\raciow\Template
 */
class NullTemplateEngine implements TemplateEngine {

	 private $cache = null;

	 public function render($path, array $data) {
			return $this->load($path)->render($data);
	 }

	 public function load($path) {
			if ($this->cache === null) {
				 $this->cache = [];
			}

			$template = new NullTemplate($path, $this);
			$this->cache[$path] = $template;
			return $template;
	 }

	 public function clearCache() {
			unset($this->cache);
	 }

}

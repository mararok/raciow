<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Template;

/** When no other implementation used.
 *  @package component\raciow\Template
 */
class NullTemplate implements Template {

	 private $name;
	 private $engine;

	 public function __construct($name, TemplateEngine $engine) {
			$this->name = $name;
			$this->engine = $engine;
	 }

	 public function render(array $data) {
			return '';
	 }

	 public function getEngine() {
			return $this->engine;
	 }

	 public function getName() {
			return $this->name;
	 }

}

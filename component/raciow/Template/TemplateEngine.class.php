<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Template;

/**
 *  @package component\raciow\Template
 */
interface TemplateEngine {

	 public function render($path, array $data);

	 public function load($path);

	 public function clearCache();
}

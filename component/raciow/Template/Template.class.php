<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Template;

/** Generic template file interface
 *  @package \component\raciow\Template
 */
interface Template {

	 /** Render template to Render Buffer with given data.
		* @param array $data Data to render.
		* @return string
		*/
	 public function render(array $data);

	 /**
		* @return \component\raciow\TemplateManger\TemplateEngine
		*/
	 public function getEngine();

	 /**
		* @return string Template file name.
		*/
	 public function getName();
}

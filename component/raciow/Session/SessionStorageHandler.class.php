<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Session;

class SessionStorageHandler implements \SessionHandlerInterface {

	 private $sessionComponent;
	 private $storage;

	 public function __construct(SessionComponent $sessionComponent) {
			$this->sessionComponent = $sessionComponent;
			$this->register();
	 }

	 private function register() {
			session_set_save_handler($this, true);
	 }

	 public function open($savePath, $sessionName) {
			return $this->storage->open($savePath, $sessionName);
	 }

	 public function close() {
			return $this->storage->close();
	 }

	 public function read($id) {
			return $this->storage->read($id);
	 }

	 public function write($id, $data) {
			return $this->storage->write($id, $data);
	 }

	 public function destroy($id) {
			return $this->storage->destroy($id);
	 }

	 public function gc($maxLifetime) {
			return $this->storage->gc($maxLifetime);
	 }

	 public function getStorage() {
			return $this->storage;
	 }

	 public function setStorage(SessionStorage $newStorage) {
			$this->storage = $newStorage;
	 }

}

?>
<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Session;

class FileSessionStorage implements SessionStorage {

	 private $savePath;

	 public function open($savePath, $sessionName) {
			$this->savePath = $savePath;
			if (!is_dir($this->savePath)) {
				 mkdir($this->savePath, 0777);
			}

			return true;
	 }

	 public function close() {
			return true;
	 }

	 public function read($id) {
			return (string) file_get_contents($this->getSessionFilePath($id));
	 }

	 public function write($id, $data) {
			return (file_put_contents($this->getSessionFilePath($id), $data) === false) ? false : true;
	 }

	 public function destroy($id) {
			$file = $this->getSessionFilePath($id);
			if (file_exists($file)) {
				 unlink($file);
			}

			return true;
	 }

	 public function gc($maxLifetime) {
			foreach (glob("$this->savePath/sess_*") as $file) {
				 if (filemtime($file) + $maxLifetime < time() && file_exists($file)) {
						unlink($file);
				 }
			}

			return true;
	 }

	 private function getSessionFilePath($id) {
			return "$this->savePath/sess_$id";
	 }

}

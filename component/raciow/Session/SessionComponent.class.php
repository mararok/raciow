<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Session;

class SessionComponent extends \raciow\component\ComponentBase {

	 private $sessionStorageHandler;
	 private $started = false;
	 private $firstWrite = true;

	 protected function _init() {
			$config = $this->getComponent('raciow.Configuration')->load('component/raciow/Session/');

			$savePath = ($config->exists('savePath')) ? $config->get('savePath') : $this->getAppBasePath() . '/sessions';
			ini_set('session.save_path', $savePath);

			$hashFunction = ($config->exists('hashFunction') && in_array($config->get('hashFunction'), hash_algos())) ? $config->get('hashFunction') : 'sha512';
			ini_set('session.hash_function', $hashFunction);

			ini_set('session.hash_bits_per_character', 5);
			ini_set('session.use_only_cookies', 1);

			$cookieParams = session_get_cookie_params();
			session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], false, true);

			$sessionName = ($config->exists('name')) ? $config->get('name') : 'RSID';
			session_name($sessionName);

			$this->sessionStorageHandler = new SessionStorageHandler($this);
			if ($config->exists('storageComponent')) {
				 $storage = $this->getComponent($config->get('storageComponent'));
				 $this->sessionStorageHandler->setStorage(($storage instanceof SessionStorage) ? $storage : new FileSessionStorage());
			}
	 }

	 public function start() {
			if ($this->started()) {
				 return;
			}

			session_start();

			if ($this->firstWrite) {
				 session_regenerate_id(true);
				 $this->firstWrite = false;
			}

			$this->started = false;
	 }

	 public function started() {
			return $this->started;
	 }

	 public function stop() {
			if (!$this->started()) {
				 session_write_close();
				 $this->started = true;
			}
	 }

	 public function destroy() {
			session_destroy();
	 }

	 public function read($key) {
			return $_SESSION[$key];
	 }

	 public function write($key, $newValue) {
			$_SESSION[$key] = $newValue;
	 }

	 public function delete($key) {
			unset($_SESSION[$key]);
	 }

}

?>
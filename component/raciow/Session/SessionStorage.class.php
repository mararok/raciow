<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace component\raciow\Session;

interface SessionStorage {

	 public function open($savePath, $sessionName);

	 public function close();

	 public function read($id);

	 public function write($id, $data);

	 public function destroy($id);

	 public function gc($maxLifetime);
}

?>
<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow;

require_once 'AutoClassLoader.class.php';

function COMPONENT_META($vendor, $name, $version) {
	 return new \raciow\component\ComponentMetadataBuilder($vendor, $name, $version);
}

class RaciowEngine {

	 const CORE_VERSION = '0.1';

	 private static $instance = null;
	 private $autoClassLoader;

	 private function __construct($basePath) {
			$this->autoClassLoader = new \AutoClassLoader($basePath);
	 }

	 public static function setupApp($basePath, $appName) {
			if (RaciowEngine::$instance == null) {
				 RaciowEngine::$instance = new RaciowEngine($basePath);
				 $app = new $appName($basePath);
				 $app->init();
				 return $app;
			}

			return new ExceptionBase("App $appName can't be setup twice ");
	 }

}

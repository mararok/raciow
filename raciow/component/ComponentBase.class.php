<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

/**
 * @package raciow
 * @subpackage component
 */
abstract class ComponentBase {

	 private $componentManager;

	 public function __construct(ComponentManager $componentManager) {
			$this->componentManager = $componentManager;
	 }

	 public function _init() {
			
	 }

	 protected function getComponent($componentName) {
			return $this->componentManager->get($componentName);
	 }

	 protected function getAppBasePath() {
			return $this->componentManager->getApp()->getBasePath();
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

class ComponentName {

	 /** var string */
	 private $vendor;

	 /** var string */
	 private $name;

	 /**
		* @param string $vendor
		* @param string $name
		*/
	 public function __construct($vendor, $name) {
			$this->vendor = $vendor;
			$this->name = $name;
	 }

	 public static function fromFullname($fullname) {
			$fullnameParts = explode('.', $fullname);
			return new ComponentName($fullnameParts[0], $fullnameParts[1]);
	 }

	 public function getVendor() {
			return $this->vendor;
	 }

	 public function getName() {
			return $this->name;
	 }

	 public function getNamespace() {
			return 'component\\' . $this->vendor . '\\' . $this->name;
	 }

	 public function getMainClassFullname() {
			return $this->getNamespace() . '\\' . $this->name . 'Component';
	 }

	 public function getPath() {
			return 'component/' . $this->vendor . '/' . $this->name;
	 }

	 public function getFullname() {
			return $this->vendor . '.' . $this->name;
	 }

	 public function __toString() {
			return $this->getFullname();
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

use \raciow\error;

/**
 * @package raciow
 * @subpackage component
 */
class ComponentException extends error\ExceptionBase {

	 /** @var \raciow\component\ComponentName */
	 private $componentName;

	 public function __construct(ComponentName $componentName, $message) {
			parent::__construct($message);
			$this->componentName = $componentName;
	 }

	 /**
		* @return \raciow\component\ComponentName
		*/
	 public function getComponentName() {
			return $this->componentName;
	 }

}

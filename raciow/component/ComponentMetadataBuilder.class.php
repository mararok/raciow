<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

class ComponentMetadataBuilder {

	 private $metadata;

	 public function __construct($vendorName, $componentName, $version) {
			$this->metadata = new ComponentMetadata($vendorName, $componentName, $version);
	 }

	 public function dependencyList(array $list) {
			$this->metadata->dependencyList = $list;
			return $this;
	 }

	 public function compatibleVersions(array $versions) {
			$this->metadata->compatibleVersions = $versions;
			return $this;
	 }

	 public function build() {
			return $this->metadata;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

use \raciow\app;

/**
 * @package raciow
 * @subpackage component
 */
class ComponentManager {

	 /** @var array */
	 private $cache = [];

	 /** @var \raciow\component\ComponentLoader */
	 private $componentLoader;

	 /** @var \raciow\app\AppBase */
	 private $app;

	 public function __construct(app\AppBase $app) {
			if (!app\AppBase::isIntegrityCheckEnabled()) {
				 $this->componentLoader = new ComponentLoader($this);
			} else {
				 $this->componentLoader = new ComponentIntegrityLoader($this);
			}

			$this->app = $app;
	 }

	 /**
		* @param string $componentName Component name format: <vendorName>.<componentName>
		* @return \raciow\component\ComponentBase
		*/
	 public function get($componentName) {

			$component = $this->cache[$componentName];
			if (!isset($component)) {
				 $component = $this->load(ComponentName::fromFullname($componentName));
			}
			return $component;
	 }

	 private function load(ComponentName $componentName) {
			$component = $this->componentLoader->load($componentName);
			$component->_init();
			$this->cache[$componentName->getFullname()] = $component;
			return $component;
	 }

	 public function getApp() {
			return $this->app;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

class ComponentMetadata {

	 public $vendorName;
	 public $componentName;
	 public $version;
	 public $dependencyList = [];
	 public $compatibleVersions = [];

	 public function __construct($vendorName, $componentName, $version) {
			$this->vendorName = $vendorName;
			$this->componentName = $componentName;
			$this->version = $version;
	 }

	 public function isCompatibleWithVersion($neededVersion) {
			return (
							$this->version == $neededVersion ||
							in_array($neededVersion, $this->compatibleVersions)
							) ? true : false;
	 }

}

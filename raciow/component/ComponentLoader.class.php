<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

/** Component main class loader
 * @package raciow
 * @subpackage component
 */
class ComponentLoader {

	 private $componentManager;

	 public function __construct(ComponentManager $componentManager) {
			$this->componentManager = $componentManager;
	 }

	 public function load(ComponentName $componentName) {
			try {
				 $componentMainClassName = $componentName->getMainClassFullname();
				 $component = new $componentMainClassName($this->componentManager);
				 return $component;
			} catch (\Exception $e) {
				 throw new ComponentNotFoundException($componentName, $e);
			}

			if (!($component instanceof ComponentBase)) {
				 throw new ComponentNotFoundException($componentName);
			}
	 }

}

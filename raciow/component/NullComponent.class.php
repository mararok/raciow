<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\component;

class NullComponent extends raciow\component\ComponentBase {

	 public function __construct(ComponentManager $componentManager) {
			parent::__construct($componentManager);
	 }

}

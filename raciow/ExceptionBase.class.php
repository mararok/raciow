<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow;

class ExceptionBase extends \Exception {

	 public function __construct($message, $previous = null) {
			parent::__construct($message, 0, $previous);
	 }

}

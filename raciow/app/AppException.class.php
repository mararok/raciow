<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\app;

class AppException extends \raciow\ExceptionBase {

	 public function __construct($message, $previous) {
			parent::__construct($message, $previous);
	 }

}

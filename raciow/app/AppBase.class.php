<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

namespace raciow\app;

use \raciow\component;

/**
 * @package raciow
 * @subpackage app
 */
abstract class AppBase {

	 private $basePath;
	 private $componentManager;

	 public function __construct($basePath) {
			$this->basePath = $basePath;
			$this->componentManager = new component\ComponentManager($this);
	 }

	 public static function isProduction() {
			return defined('APP_PRODUCTION');
	 }

	 public static function isDebug() {
			return defined('APP_DEBUG');
	 }

	 public static function isInMaintenanceMode() {
			return defined('APP_MAINTENANCE_MODE');
	 }

	 public static function isIntegrityCheckEnabled() {
			return defined('APP_INTEGRITY_CHECK');
	 }

	 public function getComponent($name) {
			return $this->componentManager->get($name);
	 }

	 public function getBasePath() {
			return $this->basePath;
	 }

}

<?php

/* Raciow Framework Project
 * The MIT License(http://opensource.org/licenses/MIT)
 * Copyright 2014  Mararok(Andrzej Wasiak, mararok@gmail.com)
 */

class AutoClassLoader {

	 const CLASS_FILE_EXT = '.class.php';

	 private $basePath;

	 public function __construct($basePath) {
			$this->basePath = $basePath . DIRECTORY_SEPARATOR;
			spl_autoload_register(array($this, 'loadClass'));
	 }

	 public function loadClass($className) {
			$classPath = str_replace('\\', DIRECTORY_SEPARATOR, $className);
			$classPath = $this->basePath . $classPath . AutoClassLoader::CLASS_FILE_EXT;
			if (file_exists($classPath)) {
				 include($classPath);
				 return true;
			} else {
				 return false;
			}
	 }

	 public function getBasePath() {
			return $this->basePath;
	 }

}
